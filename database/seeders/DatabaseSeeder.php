<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\People;
use App\Models\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        \App\Models\Transaction::factory(500)->create();
        \App\Models\People::factory(20)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


        // ini cara  memasukkan data secara manual

        // User::create([
        //     "name" => 'rizki',
        //     "username" => "tengku_rizki",
        //     "email" => "tengku@gmail.com",
        //     "password" => bcrypt('12345'),
        //     "wa_num" => "01912312312"
        // ]);
    }
}
