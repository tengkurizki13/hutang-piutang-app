<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'type' => fake()->randomElement(['depts', 'receivables']),
            'user_id' => fake()->numberBetween(1, 10),
            'user_for' => fake()->randomElement(['beli kacang', 'beli motor',"beli saos","beli tempe","beli rumah"]),
            'people_id'=> fake()->numberBetween(1, 10),
            'nominal' => fake()->numerify("###########"),
            'temp_nominal' => fake()->numerify("###########"),
            'status' => fake()->randomElement(['unpaid'])
        ];
    }
}
