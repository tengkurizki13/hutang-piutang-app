<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['depts', 'receivables']);
            $table->foreignId('user_id')->unsigned();
            $table->string('user_for');
            $table->foreignId('people_id')->unsigned();
            $table->string('nominal');
            $table->string('temp_nominal');
            $table->enum('status', ['unpaid', 'paid','installment']);
            $table->dateTime('due_date', $precision = 0)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
