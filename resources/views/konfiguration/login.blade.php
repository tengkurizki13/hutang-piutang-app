@include('partials.header')

@include('partials.topbar')

    <!-- Begin Page Content -->
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-6">

              @if (session()->has('loginError'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('loginError') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
                <form action="/login" method="POST">
                  @csrf
                    <div class="mb-3">
                      <label for="username" class="form-label">Enter Your Username</label>
                      <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" aria-describedby="emailHelp" name="username" autofocus value="{{ old('username') }}">
                      @error('username')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Enter Your Password</label>
                      <input type="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1" name="password">
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Remember Me</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                  </form>
                  <p class="mt-5">Don't have an account? <a href="/register">Register</a></p>
            </div>

        </div>
    </div>
    
    <!-- /.container-fluid -->
    @include('partials.footer')
             
