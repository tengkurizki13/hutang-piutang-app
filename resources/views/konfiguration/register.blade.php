@include('partials.header')

@include('partials.topbar')

    <!-- Begin Page Content -->
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-6">

              @if (session()->has('sukses'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('sukses') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif
              
                <form action="/register" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="mb-3">
                      <label for="username" class="form-label">Enter New Username</label>
                      <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" aria-describedby="emailHelp" value="{{ old('username') }}" required>
                      @error('username')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Enter New Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email" aria-describedby="emailHelp" value="{{ old('email') }}" required>
                        @error('email')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="wa_num" class="form-label">Enter New WhatApp</label>
                        <input type="text" class="form-control @error('wa_num') is-invalid @enderror" name="wa_num" id="wa_num" aria-describedby="emailHelp" value="{{ old('wa_num') }}" required>
                        @error('wa_num')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                @enderror
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label ">Enter New Password</label>
                      <input type="password" class="form-control @error('password') is-invalid @enderror " name="password" id="exampleInputPassword1" required>
                      @error('password')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                    </div>

                    <div class="mb-3">
                      <label for="avatar" class="form-label">Upload Image</label>
                      <img class="img-preview img-fluid my-3" width="400">
                      <input class="form-control @error('avatar') is-invalid @enderror" type="file" id="avatar" name="avatar" onchange="previewImage()">
                      @error('avatar')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                      @enderror
                  </div>

                   
                    <button type="submit" class="btn btn-primary">Register</button>
                  </form>
                  <p class="mt-5">Already account? <a href="/login">login</a></p>
            </div>

        </div>
    </div>

    <script>

      function previewImage() {
              const avatar = document.querySelector('#avatar');
              const imgPreview = document.querySelector('.img-preview');
      
              imgPreview.style.display = 'block';
      
              const oFReader = new FileReader();
              oFReader.readAsDataURL(avatar.files[0]);
              
      
              oFReader.onload = function(oFREvent){
              imgPreview.src = oFREvent.target.result;
              }
          }

  </script>
    
    <!-- /.container-fluid -->
    @include('partials.footer')
             
