  <!-- Sidebar -->
  <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon ">
            <i class="bi bi-bank"></i>
        </div>
        <div class="sidebar-brand-text mx-3">hutang piutang-app</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/dashboard">
            <i class="bi bi-journals"></i>
            <span>Dashboard</span>
        </a>
    </li>

   
    <!-- Nav Item - Pages Collapse Menu -->
    @auth


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Transactions
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="/hutang" 
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-currency-dollar"></i>
            <span>Hutang</span>
        </a>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="/piutang" 
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="bi bi-currency-exchange"></i>
            <span>Piutang</span>
        </a>
    </li>


    @else   
    
    @endauth


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
