
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card mb-3">
                  <div class="card-body p-4">
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Orang</h6>
                      </div>

                      <div class="col-sm-9 text-secondary">
                          {{ $people[0]->name }}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Type</h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                          {{ $hutang->type }}
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Kegunaan</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->user_for }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Nominal</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->nominal }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Jumlah angsuran</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->temp_nominal }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Waktu Transaksi</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->created_at }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Jatuh Tempo</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->due_date }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Status</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ $piutang->status }}
                        </div>
                      </div>
                      <hr>
                    <div class="row p-1">
                      <div class="col-sm-12">
                          <a href="/piutang" class="btn btn-danger">Kembali</a>
                          
                      </div>
                    </div>
                  </div>
                </div>
          </div>
        </div>
    </div>
 @endsection
             