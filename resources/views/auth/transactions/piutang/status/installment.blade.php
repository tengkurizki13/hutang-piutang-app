
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
        <div class="container">
                <div class="row">
        <h1>Installment</h1>
        <div class="col-6">
        <form action="/status" method="post">
            @csrf
                <div class="mb-3">
                    <label for="temp_nominal" class="form-label">Installment_Nominal</label>
                    <input type="text" class="form-control @error('temp_nominal') is-invalid @enderror" name="temp_nominal"
                        id="temp_nominal">
                        @error('temp_nominal')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                   @enderror
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Jumlah Hutang Awal</th>
                            <td>{{ $transactions[0]['nominal'] }}</td>
                        </tr>
                        <tr>
                            <th>Jumlah Angsuran</th>
                            <td>{{ $transactions[0]['temp_nominal'] }}</td>
                        </tr>
                        <tr>
                            <th>Sisa Angsuran</th>
                            <td>{{ $transactions[0]['nominal'] - $transactions[0]['temp_nominal'] }}</td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" name="id" value="{{ $transactions[0]['id'] }}">
                <input type="hidden" name="oldValue" value="{{ $transactions[0]['temp_nominal'] }}">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-danger" href="/hutang">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection