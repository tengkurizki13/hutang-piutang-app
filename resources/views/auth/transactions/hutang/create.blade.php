
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
        <div class="container">
            <div class="row">
            

                <div class="col-md-12 d-flex justify-content-center">
                    <form action="/hutang" method="post">
                        @csrf

                    <div class="mb-3">
                        <label for="person_id" class="form-label">Pemberi Hutang</label>
                        <a href="/people/hutang/create" class="badge bg-danger">tambah</a>
                        <select class="form-select" name="people_id">
                            <option value="">pilih orang</option>
                            @foreach ($people as $p)
                            @if (old('people_id') == $p->id)
                            <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                            @else
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                            @endif
                            @endforeach
                          </select>
                        @error('people_id')
                            <div class="invalid-feedback">
                              {{ $message }}
                            </div>
                         @enderror
                        </div>

                    <div class="mb-3">
                        <label for="user_for" class="form-label">Kegunaan</label>
                        <input type="text" class="form-control @error('user_for') is-invalid @enderror" id="user_for" value="{{ old('user_for') }}" name="user_for">
                        @error('user_for')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                        <label for="nominal" class="form-label">Nominal</label>
                        <input type="text" class="form-control @error('nominal') is-invalid @enderror" id="nominal" value="{{ old('nominal') }}" name="nominal">
                        @error('nominal')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                      <label for="created_at" class="form-label">Waktu Hutang</label>
                      <input type="datetime-local" class="form-control @error('created_at') is-invalid @enderror" id="created_at" value="{{ old('created_at') }}" name="created_at">
                      @error('created_at')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                   @enderror
                  </div>

                  <div class="mb-3">
                      <label for="due_date" class="form-label">Kapan Bayar</label>
                      <input type="datetime-local" class="form-control @error('due_date') is-invalid @enderror" id="due_date" value="{{ old('due_date') }}" name="due_date">
                      @error('due_date')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                   @enderror
                  </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/hutang" class="btn btn-warning">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    @endsection
             