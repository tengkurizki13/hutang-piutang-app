
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
        <div class="container">
    <!-- Search -->
        <div class="row mt-5 mb-3">
            <div class="col-md-5 d-flex justify-content-end">
                <form action="/hutang"
                class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" >
                <div class="input-group">
                 <input type="text" name="search" class="form-control bg-light border-0 small" placeholder="Search for..."
                     value="{{ request('search') }}">
                 <div class="input-group-append">
                     <button class="btn btn-primary" type="submit">
                         <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-2 d-flex justify-content-start dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Filter
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" href="/hutang?action=unpaid">Unpaid</a></li>
                <li><a class="dropdown-item" href="/hutang?action=paid">Paid</a></li>
                <li><a class="dropdown-item" href="/hutang?action=installment">Installment</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a class="dropdown-item" href="/hutang">Clear Filter</a></li>
              </ul>
        </div>

        <div class="col-md-5 d-flex justify-content-end">
            <a href="/hutang/create" class="btn btn-secondary"><i class="bi bi-plus-circle"></i> Tambah Hutang</a>
        </div>
        </div>
        
        <!-- trancation -->
        @if (session()->has('sukses'))
        <div class="alert alert-success alert-dismissible fade show me-3" role="alert">
          {{ session('sukses') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    <div class="row">
        <div class="col-md-12">
                <table class="table" style="margin-top: 10px; width: 100%; ">
                    <thead class="bg-white">
                        <tr style="font-family: bold">
                            <th>No</th>
                            <th>Name</th>
                            <th>@sortablelink('user_for','Kegunaan')</th>
                            <th>Nominal</th>
                            <th>Status</th>
                            <th>Jatuh Tempo</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($transactions->count())
                        @foreach ($transactions as $key => $transaction)
                        <tr style="font-size: 12px">
                            <td>{{ $key + $transactions->firstItem() }}</td>
                            <td>{{ $transaction->people->name }}</td>
                            <td>{{ $transaction->user_for }}</td>
                            <td>@rupiah($transaction->temp_nominal == 0 ? $transaction->nominal : ($transaction->nominal - $transaction->temp_nominal))</td>
                            <td><div class="dropdown">
                                <button class="badge bg-{{ $transaction->status == 'unpaid' ? 'danger' : ($transaction->status == 'paid' ? 'success' : 'warning') }} dropdown-toggle border-0" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    {{ $transaction->status }}
                                </button>


                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  <li><a class="dropdown-item" href="/status/{{ $transaction->status == 'unpaid' || $transaction->status == 'installment' ? 'paid' : 'unpaid' }}/{{ $transaction->id }}/hutang">
                                    {{ $transaction->status == 'unpaid' || $transaction->status == 'installment' ? 'paid' : 'unpaid' }}
                                </a></li>
                                @if ($transaction->status == 'unpaid' || $transaction->status == 'installment')
                                <li><a class="dropdown-item" href="/status/installment/{{ $transaction->id }}/hutang">Installment</a></li>
                                @endif
                                </ul>
                              </div></td>
                            <td>{{ $transaction->created_at }}</td>
                            <td> 
                                <a href="/hutang/{{ $transaction->id }}" class="badge bg-info">detail</a>
                                <a href="/hutang/{{ $transaction->id }}/edit" class="badge bg-warning">edit</a>
                                <form action="/hutang/{{ $transaction->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="badge bg-danger border-0" onclick="return confirm('yakin Boss??')">hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">
                                Data Kosong
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                <div class="d-flex justify-content-start">
                    {{ $transactions->onEachSide(1)->links() }}
                </div>
            </div>
        </div>

    <!-- /.container-fluid -->
    </div>
  
          
</div>
 @endsection
             