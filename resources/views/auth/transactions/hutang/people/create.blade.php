
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 d-flex justify-content-center">
                    <form action="/people/hutang" method="post">
                        @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">New Person</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}" name="name">
                        @error('name')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                        <label for="wa_num" class="form-label">No WhatApp</label>
                        <input type="text" class="form-control @error('wa_num') is-invalid @enderror" id="wa_num" value="{{ old('wa_num') }}" name="wa_num">
                        @error('wa_num')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/hutang/create" class="btn btn-warning">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    @endsection
             