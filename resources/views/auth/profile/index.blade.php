
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
    <div class="container">

        <div class="main-body">

          @if (session()->has('sukses'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('sukses') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif

              <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex flex-column align-items-center text-center">
                        @if (auth()->user()->avatar)
                        <img src="{{ asset('storage/' . auth()->user()->avatar) }}" alt="Admin" class="" width="200" height="150">
                        @else
                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                        @endif
                        <div class="mt-3">
                          <h4>{{ auth()->user()->username }}</h4>
                          <p class="text-secondary mb-1">Full Stack Developer</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="card mb-3">
                    <div class="card-body p-4">
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Full Name</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ auth()->user()->username }}
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Email</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ auth()->user()->email }}
                        </div>
                      </div>
                      <hr>
                      <div class="row ">
                        <div class="col-sm-3">
                          <h6 class="mb-0">Phone</h6>
                        </div>
                        <div class="col-sm-9 text-secondary">
                            {{ auth()->user()->wa_num }}
                        </div>
                      </div>
                      <hr>
                      <div class="row p-1">
                        <div class="col-sm-12">
                            <a href="/profile/{{ auth()->user()->id }}/edit" class="btn btn-secondary">edit</a>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>

        </div>

    </div>

 @endsection
         
