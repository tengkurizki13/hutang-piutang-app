
 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 d-flex justify-content-center">
                    <form action="/profile/{{ $profile->id }}" method="post" enctype="multipart/form-data">
                      @method('put')
                        @csrf
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" value="{{ old('username',$profile->username) }}" name="username">
                        @error('username')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email',$profile->email) }}" name="email">
                        @error('email')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                        <label for="wa_num" class="form-label">Phone</label>
                        <input type="text" class="form-control @error('wa_num') is-invalid @enderror" id="wa_num" value="{{ old('wa_num',$profile->wa_num) }}" name="wa_num">
                        @error('wa_num')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <div class="mb-3">
                        <label for="avatar" class="form-label d-block">Upload Avatar</label>
                        <input type="hidden" name="oldImage" value="{{ $profile->avatar }}">
                        @if ($profile->avatar)
                        <img src="{{ asset('storage/'. $profile->avatar) }}" class="img-preview img-fluid my-3" width="400">
                        @else    
                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png" class="img-preview img-fluid my-3" width="400">
                        @endif
                        <input class="form-control @error('avatar') is-invalid @enderror" type="file" id="avatar" name="avatar" onchange="previewImage()">
                        @error('avatar')
                        <div class="invalid-feedback">
                          {{ $message }}
                        </div>
                     @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/profile"  class="btn btn-danger">Kembali</a>
                </form>
            </div>
        </div>
    </div>

    <script>

        function previewImage() {
                const avatar = document.querySelector('#avatar');
                const imgPreview = document.querySelector('.img-preview');
        
                imgPreview.style.display = 'block';
        
                const oFReader = new FileReader();
                oFReader.readAsDataURL(avatar.files[0]);
                
        
                oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
                }
            }
  
    </script>

  
  
    @endsection
             