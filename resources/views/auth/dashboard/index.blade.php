 @extends('layouts.main')

 @section('container')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src="https://source.unsplash.com/1200x800?{{ 'money' }}" class="card-img-top" alt="...">
                    <div class="card-body text-center">
                      <h5>Total Debt</h5>
                      <p>@rupiah($dept)</p>
                    </div>
                  </div>
            </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img src="https://source.unsplash.com/1200x800?{{ 'dollar' }}" class="card-img-top" alt="...">
                  <div class="card-body text-center">
                    <h5>Total Receivables</h5>
                    <p>@rupiah($receivables)</p>
                  </div>
                </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img src="https://source.unsplash.com/1200x800?{{ 'dept' }}" class="card-img-top" alt="...">
                <div class="card-body text-center">
                  <h5>Debt Transactions</h5>
                  <p>{{ $deptTransaction }}</p>
                </div>
              </div>
        </div>
        </div>
        <div class="row mt-2">
          <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img src="https://source.unsplash.com/1200x800?{{ 'receivables' }}" class="card-img-top" alt="...">
                  <div class="card-body text-center">
                    <h5>Receivables Transactions</h5>
                    <p>{{ $receivableTransaction }}</p>
                  </div>
                </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img src="https://source.unsplash.com/1200x800?{{ 'arabian dept' }}" class="card-img-top" alt="...">
                <div class="card-body text-center">
                  <h5>People Most Debt</h5>
                  <p>{{ $namemostDeptPeople }}</p>
                </div>
              </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img src="https://source.unsplash.com/1200x800?{{ 'arabian receivables' }}" class="card-img-top" alt="...">
              <div class="card-body text-center">
                <h5>People Most Receivables</h5>
                <p>{{ $namemostreceivablesPeople }}</p>
              </div>
            </div>
      </div>
      </div>
    </div>
    <!-- /.container-fluid -->
 @endsection
             