<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Transaction extends Model
{
    use HasFactory;
    use Sortable;

    protected $guarded = ['id'];

    public $sortable = ['user_for'];


    public function scopeFilter($query,$filter)
    {

        $query->when($filter ?? false, function($query, $filter) {
            return $query->where('status',$filter);
         });
    }

    public function scopeSearch($query,array $searches,$id)
    {
        

        $query->when($searches['search'] ?? false, function($query, $search) use ($id) {
            return $query->where('user_for','like','%' . $search . '%')
                        ->orWhere('status','like','%' . $search . '%')
                        ->orWhere('type','like','%' . $search . '%')
                        ->orWhere('people_id','like','%' . $id . '%');
         });
    }


    public function people()
    {
        return $this->belongsTo(People::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
