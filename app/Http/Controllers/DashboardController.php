<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\People;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    
    public function index()
    {
        $mostDeptPeople = DB::table('transactions')
        ->select(DB::raw('people_id,count(*)'))
        ->where('user_id',auth()->user()->id)
        ->where('type','depts')
        ->groupBy('people_id')
        ->orderBy('count(*)','desc')
        ->first();

        if ($mostDeptPeople != null) {
            $namemostDeptPeople = People::where('id',$mostDeptPeople->people_id)->get();
        }

        $mostreceivablesPeople = DB::table('transactions')
        ->select(DB::raw('people_id,count(*)'))
        ->where('user_id',auth()->user()->id)
        ->where('type','receivables')
        ->groupBy('people_id')
        ->orderBy('count(*)','desc')
        ->first();
        
        if ($mostreceivablesPeople != null) {
            $namemostreceivablesPeople = People::where('id',$mostreceivablesPeople->people_id)->get();
        }

 
        return view('auth.dashboard.index',[
            'dept' => Transaction::where('user_id',auth()->user()->id)->where('type','depts')->sum('nominal'),
            'receivables' => Transaction::where('user_id',auth()->user()->id)->where('type','receivables')->sum('nominal'),
            'deptTransaction' => Transaction::where('user_id',auth()->user()->id)->where('type','depts')->count(),
            'receivableTransaction' => Transaction::where('user_id',auth()->user()->id)->where('type','receivables')->count(),
            'namemostDeptPeople' => $namemostDeptPeople[0]->name ?? 0,
            'namemostreceivablesPeople' => $namemostreceivablesPeople[0]->name ?? 0,

        ]);
    }
}
