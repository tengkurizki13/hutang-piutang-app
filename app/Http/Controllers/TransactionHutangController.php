<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class TransactionHutangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $namePeople = People::where('user_id',auth()->user()->id)
                            ->where('name','like','%' . request('search') . '%')
                            ->get();

        if ($namePeople->count() !== 0) {
            $id = $namePeople[0]->id;
        }


        return view('auth.transactions.hutang.index',[
            'transactions' => Transaction::where('user_id',auth()->user()->id)->where('type','depts')->filter(request('action'))->search(request(['search']),$id = 0)->sortable()->paginate(5)->withQueryString()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.transactions.hutang.create',[
            'people' => People::where('user_id',auth()->user()->id)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'people_id' => 'required|max:200',
            'user_for' => 'required|max:255',
            'nominal' => 'required|numeric',
            'created_at' => 'required',
            'due_date' => 'required'
        ]);


        $replaced_created_at = Str::replace("T", " ", $request->created_at);
        $adjusted_created_at = Str::finish( $replaced_created_at, ':00');  
        $replaced_due_date = Str::replace("T", " ", $request->due_date);
        $adjusted_due_date = Str::finish( $replaced_due_date, ':00');

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['type'] = "depts";
        $validatedData['temp_nominal'] = 0;
        $validatedData['status'] = "unpaid";
        $validatedData['created_at'] = $adjusted_created_at;
        $validatedData['due_date'] = $adjusted_due_date;


        Transaction::create($validatedData);

        return redirect('/hutang')->with('sukses','New Transaction has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $hutang)
    {

        return view("auth.transactions.hutang.show",[
            'hutang' => $hutang,
            'people' => People::where('id',$hutang->people_id)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $hutang)
    {
        return view('auth.transactions.hutang.edit',[
            'people' => People::where('user_id',auth()->user()->id)->get(),
            'transaction' => $hutang,
        ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $hutang)
    {
        $validatedData = $request->validate([
            'people_id' => 'required|max:200',
            'user_for' => 'required|max:255',
            'nominal' => 'required|numeric',
            'created_at' => 'required',
            'due_date' => 'required'
        ]);


        $replaced_created_at = Str::replace("T", " ", $request->created_at);
        $adjusted_created_at = Str::finish( $replaced_created_at, ':00');  
        $replaced_due_date = Str::replace("T", " ", $request->due_date);
        $adjusted_due_date = Str::finish( $replaced_due_date, ':00');

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['type'] = "depts";
        $validatedData['temp_nominal'] = 0;
        $validatedData['status'] = "unpaid";
        $validatedData['created_at'] = $adjusted_created_at;
        $validatedData['due_date'] = $adjusted_due_date;


        Transaction::where('id', $hutang->id)->update($validatedData);

        return redirect('/hutang')->with('success','New Transaction has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $hutang)
    {
        Transaction::destroy($hutang->id);

        return redirect('/hutang')->with('success','New Transaction has been deleted');
    }
}
