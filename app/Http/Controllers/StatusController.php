<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;

class StatusController extends Controller
{
    public function paid($status,$id,$transaction)
    {

        if ($status == 'installment') {
            return view('auth.transactions.hutang.status.installment',[
                'transactions' => Transaction::where('id',$id)->get()
            ]);
        }

        $nominal = Transaction::where('id',$id)->get();

        if ($status == 'unpaid') {
            $temp_nominal = 0;
        }else{
            $temp_nominal = $nominal[0]->nominal;
        }

        $updateStatus =[
            'status' => $status,
            'temp_nominal' => $temp_nominal
        ];



        Transaction::where('id', $id)->update($updateStatus);

        return redirect('/'.$transaction)->with('sukses','New Status has been updated');
    }


    public function installment(Request $request)
    {

        
        $updateStatus = $request->validate([
            'temp_nominal' => 'required|numeric',
            
        ]);
        
        $temp_nominal = $request->temp_nominal + $request->oldValue;
        
        $updateStatus =[
            'temp_nominal' => $temp_nominal,
            'status' => 'installment'
        ];


        Transaction::where('id', $request->id)->update($updateStatus);

        return redirect('/status/installment/'. $request->id)->with('sukses','New Transaction has been updated');

    }
}
