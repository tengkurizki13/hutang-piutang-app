<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegisterController extends Controller
{
    public function index()
    {
        return view('konfiguration.register');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255|email:rfc,dns',
            'wa_num' => 'required|unique:users|min:11|numeric',
            'password' => 'required',
            'avatar' => 'image|file|max:1024'
        ]);

        if( $request->file('avatar')){
        $validatedData['avatar'] = $request->file('avatar')->store('post-images');
        }
        
        // $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['password'] = Hash::make($validatedData['password']);


        User::create($validatedData);
        // $request->session()->flash('sukses',"Registrasi Berhasil");
        return redirect('/register')->with('sukses',"Registrasi Berhasil");

    }
}
