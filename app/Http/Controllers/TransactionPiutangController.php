<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransactionPiutangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.transactions.piutang.index',[
            'transactions' => Transaction::where('user_id',auth()->user()->id)->where('type','receivables')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.transactions.piutang.create',[
            'people' => People::where('user_id',auth()->user()->id)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'people_id' => 'required|max:200',
            'user_for' => 'required|max:255',
            'nominal' => 'required|numeric',
            'created_at' => 'required',
            'due_date' => 'required'
        ]);


        $replaced_created_at = Str::replace("T", " ", $request->created_at);
        $adjusted_created_at = Str::finish( $replaced_created_at, ':00');  
        $replaced_due_date = Str::replace("T", " ", $request->due_date);
        $adjusted_due_date = Str::finish( $replaced_due_date, ':00');

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['type'] = "receivables";
        $validatedData['temp_nominal'] = 0;
        $validatedData['status'] = "unpaid";
        $validatedData['created_at'] = $adjusted_created_at;
        $validatedData['due_date'] = $adjusted_due_date;


        Transaction::create($validatedData);

        return redirect('/piutang')->with('sukses','New Transaction has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $piutang)
    {

        return view("auth.transactions.piutang.show",[
            'piutang' => $piutang,
            'people' => People::where('id',$piutang->people_id)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $piutang)
    {
        return view('auth.transactions.piutang$piutang.edit',[
            'people' => People::where('user_id',auth()->user()->id)->get(),
            'transaction' => $piutang,
        ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $piutang)
    {
        $validatedData = $request->validate([
            'people_id' => 'required|max:200',
            'user_for' => 'required|max:255',
            'nominal' => 'required|numeric',
            'created_at' => 'required',
            'due_date' => 'required'
        ]);


        $replaced_created_at = Str::replace("T", " ", $request->created_at);
        $adjusted_created_at = Str::finish( $replaced_created_at, ':00');  
        $replaced_due_date = Str::replace("T", " ", $request->due_date);
        $adjusted_due_date = Str::finish( $replaced_due_date, ':00');

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['type'] = "receivables";
        $validatedData['temp_nominal'] = 0;
        $validatedData['status'] = "unpaid";
        $validatedData['created_at'] = $adjusted_created_at;
        $validatedData['due_date'] = $adjusted_due_date;


        Transaction::where('id', $piutang->id)->update($validatedData);

        return redirect('/piutang')->with('success','New Transaction has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $piutang)
    {
        Transaction::destroy($piutang->id);

        return redirect('/piutang')->with('success','New Transaction has been deleted');
    }
}
