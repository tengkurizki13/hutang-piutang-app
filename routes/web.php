<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PeopleController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\PeopleHutangController;
use App\Http\Controllers\PeoplePiutangController;
use App\Http\Controllers\TransactionHutangController;
use App\Http\Controllers\TransactionPiutangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[LoginController::class,"index"]);
Route::get('/dashboard',[DashboardController::class,"index"])->middleware('auth');

Route::get('/login',[LoginController::class,"index"])->name('login')->middleware('guest');
Route::post('/login',[LoginController::class,"authenticate"]);
Route::get('/logout',[LoginController::class,"logout"]);

Route::get('/register',[RegisterController::class,"index"])->middleware('guest');
Route::post('/register',[RegisterController::class,"store"]);

Route::resource('/piutang', TransactionPiutangController::class)->middleware('auth');
Route::resource('/hutang', TransactionHutangController::class)->middleware('auth');


Route::resource('/profile', ProfileController::class)->middleware('auth');


Route::resource('/people/hutang', PeopleHutangController::class)->middleware('auth');
Route::resource('/people/piutang', PeoplePiutangController::class)->middleware('auth');


Route::get('/status/{status}/{id}/{transaction}',[StatusController::class,"paid"])->middleware('auth');

Route::post('/status',[StatusController::class,"installment"])->middleware('auth');
